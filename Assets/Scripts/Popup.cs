using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Popup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _label;

    private float _timer;

    public Action<Popup> OnTimeOver;

    public void Init(Action<Popup> listener, string text)
    {
        if (OnTimeOver == null)
            OnTimeOver = listener;

        _timer = 0;

        _label.SetText(text);
    }

    //����� ������� ����� ���������, ��������, �� � �� ��������� DOTween
    public void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > 2f)
        {
            OnTimeOver.Invoke(this);
            gameObject.SetActive(false);
        }
    }
}
