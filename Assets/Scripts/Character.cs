using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public event Action OnDeath;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (OnDeath != null)
            OnDeath.Invoke();
    }
}
