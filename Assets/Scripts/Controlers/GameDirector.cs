using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
    [SerializeField] private Character _character;
    [SerializeField] private BikeController _bikeController;
    [SerializeField] private LevelCreator _levelCreator;
    [SerializeField] private PopupManager _popupManager;
    [SerializeField] private TextMeshProUGUI _distanceLabel;

    private void Start()
    {
        _character.OnDeath += Restart;
        _bikeController.Init(_levelCreator.CreateBlock);
        _levelCreator.StartCreate();
        _popupManager.Init();
    }

    private void FixedUpdate()
    {
        var distance = Mathf.Round(_bikeController.DistanceCovered);
        _distanceLabel.SetText($"Distance: {distance.ToString()}");
    }

    private void Restart()
    {
        PopupManager.Instance.GetOrCreatePopup("OOOOOPS");
        _character.OnDeath -= Restart;
        StartCoroutine(RestartCoroutine());
    }

    private IEnumerator RestartCoroutine()
    {
        yield return new WaitForSeconds(2f);
        _bikeController.Restart();
        _levelCreator.StartCreate();
        _character.OnDeath += Restart;
    }
}
