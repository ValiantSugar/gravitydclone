using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeController : MonoBehaviour
{
    public const int DISTANCE_FOR_CREATE_BLOCK = 100;

    [SerializeField] private Rigidbody2D _bodyRigidbody;
    [SerializeField] private WheelJoint2D _backWheel;
    [SerializeField] private WheelJoint2D _frontWheel;
    [SerializeField] private CircleCollider2D _backWheelCollider;
    [SerializeField] private CircleCollider2D _frontWheelCollider;
    [SerializeField] private Rigidbody2D _frontWheelRigidBody;
    [SerializeField] private float _motorPower;
    [SerializeField] private LayerMask _groundLayerMask;

    private Transform _bodyTransform;

    private Transform _backWheelTransform;
    private Transform _frontWheelTransform;
    private float _backWheelRadius;
    private float _frontWheelRadius;

    private Vector3 _lastBodyRotation;
    private float _distanceFromStartingPoint;
    private float _nextCheckPoint;

    private bool _wheelieIsExist;

    public float DistanceCovered => _distanceFromStartingPoint;

    private Action OnReachCheckPoint;

    public void Init(Action listener)
    {
        _bodyTransform = transform;

        _backWheelTransform = _backWheel.connectedBody.transform;
        _frontWheelTransform = _frontWheelRigidBody.transform;
        _backWheelRadius = _backWheelCollider.radius;
        _frontWheelRadius = _frontWheelCollider.radius;

        _nextCheckPoint = LevelCreator.BLOCK_WIDTH;

        OnReachCheckPoint = listener;
    }

    private void FixedUpdate()
    {
        TryMove();
        CheckBrake();

        _distanceFromStartingPoint = _bodyTransform.position.x;
        if (_distanceFromStartingPoint > _nextCheckPoint)
        {
            _nextCheckPoint += LevelCreator.BLOCK_WIDTH;
            OnReachCheckPoint.Invoke();
        }
    }

    private void TryMove()
    {
        var speed = -Input.GetAxis("Horizontal") * _motorPower;
        if (speed == 0)
        {
            _backWheel.useMotor = false;
        }
        else
        {
            _backWheel.useMotor = true;
            var newMotor = new JointMotor2D() { motorSpeed = speed, maxMotorTorque = _backWheel.motor.maxMotorTorque, };
            _backWheel.motor = newMotor;
        }
    }

    private void CheckBrake()
    {
        var isBrake = Input.GetKey(KeyCode.Space);
        _frontWheel.useMotor = isBrake;

        var hit = Physics2D.CircleCast(_frontWheelTransform.position, _frontWheelRadius + 0.02f, Vector2.zero, 0f, _groundLayerMask);
        var frontWheelIsGround = hit.collider != null;

        if (frontWheelIsGround)
        {
            _wheelieIsExist = false;

            _lastBodyRotation = _bodyTransform.rotation.eulerAngles;

            if (isBrake)
                _frontWheelRigidBody.velocity = Vector2.zero;

            return;
        }

        if (!_wheelieIsExist)
            CheckWheelie();
    }

    private void CheckWheelie()
    {
        var hit = Physics2D.CircleCast(_backWheelTransform.position, _backWheelRadius + 0.02f, Vector2.zero, 0f, _groundLayerMask);
        var backWheelIsGround = hit.collider != null;

        if (!backWheelIsGround)
            return;

        var diff = Mathf.Abs(_lastBodyRotation.z - _bodyTransform.rotation.eulerAngles.z);
        if (diff > 45f)
        {
            _wheelieIsExist = true;
            PopupManager.Instance.GetOrCreatePopup("Wheelie!");
        }
    }

    public void Restart()
    {
        _bodyTransform.position = _bodyTransform.parent.position;
        _bodyTransform.rotation = Quaternion.identity;
        _bodyRigidbody.velocity = Vector2.zero;
        _bodyRigidbody.freezeRotation = true;
        _bodyRigidbody.isKinematic = true;
        StartCoroutine(RestartCoroutine());
        _backWheel.useMotor = false;
        _frontWheel.useMotor = false;

        _nextCheckPoint = LevelCreator.BLOCK_WIDTH;
    }

    private IEnumerator RestartCoroutine()
    {
        yield return new WaitForSeconds(1f);
        _bodyRigidbody.freezeRotation = false;
        _bodyRigidbody.isKinematic = false;
    }
}
