using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreator : MonoBehaviour
{
    public const int VISIBLE_BLOCKS_COUNT = 10;
    public const float BLOCK_WIDTH = 29.5f;

    [SerializeField] private Transform[] _blocksPrefabs;

    private Queue<Transform> _usedBlocks;
    private List<Transform> _blocksPool;
    private Vector2 _nextBlockPosition;

    public void StartCreate()
    {
        if (_usedBlocks == null)
        {
            _usedBlocks = new Queue<Transform>();
            _blocksPool = new List<Transform>();
        }
        else
        {
            while (_usedBlocks.Count > 0)
            {
                var block = _usedBlocks.Dequeue();
                _blocksPool.Add(block);
                block.gameObject.SetActive(false);
            }

            _usedBlocks.Clear();
        }

        _nextBlockPosition = Vector2.zero;

        for (var i = 0; i < VISIBLE_BLOCKS_COUNT; i++)
            CreateBlock();
    }

    public void CreateBlock()
    {
        if (_usedBlocks.Count > 10)
        {
            var previousBlock = _usedBlocks.Dequeue();
            _blocksPool.Add(previousBlock);
            previousBlock.gameObject.SetActive(false);
        }

        Transform newBlock;
        if (_blocksPool.Count > 10)
        {
            var randomIndex = Random.Range(0, _blocksPool.Count);
            newBlock = _blocksPool[randomIndex];
            _blocksPool.RemoveAt(randomIndex);

            newBlock.gameObject.SetActive(true);
        }
        else
        {
            var randomBlock = _blocksPrefabs[Random.Range(0, _blocksPrefabs.Length)];
            newBlock = Instantiate(randomBlock, transform);
        }

        newBlock.position = _nextBlockPosition;
        _nextBlockPosition += new Vector2(BLOCK_WIDTH, 0);
        _usedBlocks.Enqueue(newBlock);
    }
}
