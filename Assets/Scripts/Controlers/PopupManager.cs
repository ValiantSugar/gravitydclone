using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    public static PopupManager Instance;

    [SerializeField] private Popup _popupPrefab;

    private Queue<Popup> _popupsPool;

    public void Init()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        _popupsPool = new Queue<Popup>();
    }

    public void GetOrCreatePopup(string text)
    {
        Popup popup;
        if (_popupsPool.Count == 0)
        {
            popup = Instantiate(_popupPrefab, transform);
        }
        else
        {
            popup = _popupsPool.Dequeue();
        }

        popup.Init(ReturnToPool, text);
    }

    private void ReturnToPool(Popup popup)
    {
        _popupsPool.Enqueue(popup);
    }
}
